\documentclass[12pt]{article}
\usepackage{../tp}
\usepackage{wrapfig}
\usetikzlibrary{arrows}

\def\title{TP \no{} 5 -- Arbres couvrants minimaux aussi}

\begin{document}
\header

\section{Oups !}

Yorel Reivax ne trouve plus son chat, et décide d'exécuter le code suivant pour en savoir plus :
\begin{lstlisting}
if false then
	if true then print_string "Le chat est vivant."
else
	print_string "Le chat est mort."
;;
\end{lstlisting}
Alors ? Bref, n'oubliez pas : chaque fois que vous oubliez une parenthèse, Dieu ne tue pas un chat.

\section{Union-find}

\subsection{Préliminaires}

On cherche à partitionner $\{0, \ldots, n - 1\}$. Pour ce faire, nous requérons une structure qui nous permette efficacement de :
\begin{itemize}
\item \textbf{trouver} la partition à laquelle appartient un élément ;
\item \textbf{unir} deux partitions.
\end{itemize}\bigskip

Nous allons considérer une forêt d'arbres enracinés\footnote{Un arbre enraciné est un arbre simplement connexe tel que tous les nœuds ont un unique parent.}. Chaque arbre représentera une partition\footnote{Deux éléments seront dans le même arbre si et seulement s'ils sont dans la même partition.}, et il est naturel d'unir deux partitions en accrochant la racine d'un arbre à la racine de l'autre.

\begin{figure}[h]
\centering
\begin{tikzpicture}[>=stealth,
	every loop/.style={out=105,in=75,looseness=0.15,->},
	level 1/.style={sibling distance=1.6cm},
	level 2/.style={sibling distance=1.2cm},
	every node/.style={draw,circle},
	edge from parent/.style={draw,<-}]
\node (a) {0}
	child {node {1}
		child {node {3}}
		child {node (c) {4}}
	};
\node (b) at (2,0) {2}
	child {node {5}};
\draw (c) edge[dashed, bend right=20,->] node[auto,swap,draw=none] {\footnotesize Find} (a);
\draw (a) edge[loop,out=105,in=75,looseness=4.2,->] (a);
\draw (b) edge[loop,out=105,in=75,looseness=4.2,->] (b);
\begin{scope}[xshift=6cm]
\node (d) {0}
	child {node {1}
		child {node {3}}
		child {node {4}}
	}
	child {node {2}
		child {node[solid] {5} edge from parent [solid]}
		edge from parent[dashed] node[near start,right,draw=none] {\footnotesize Union}
	};
\draw (d) edge[loop,out=105,in=75,looseness=4.2,->] (d);
\end{scope}
\end{tikzpicture}
\caption{À gauche, une représentation des partitions \{0, 1, 3, 4\} et \{2, 5\}. Le père de 4 est 1, et 0 et 2 sont les \emph{représentants} de chaque partition (ils sont en particulier leurs propres pères). À droite, leur union.}
\end{figure}

Il convient de représenter cette structure en mémoire par un tableau \texttt{pere} de  $n$ éléments, où \texttt{pere.(i)} est le numéro du père de \texttt{i}.

\question
Quelle est la complexité des opérations \textbf{trouver} et \textbf{unir} dans le pire cas ?\bigskip

On souhaite donc améliorer l'efficacité de notre structure en s'aidant de deux astuces :
\begin{itemize}
\item la \emph{compression de chemin}, le concept d'aplatir la structure d'arbre au fur et à mesure qu'on cherche le représentant d'un sommet, en reliant tous les sommets parcourus à la racine ;
\item le \emph{rang} d'un sommet : une estimation de la hauteur de l'arbre enraciné en ce sommet.
\end{itemize}

\begin{lstlisting}
type unionfind = {pere : int vect; rang : int vect};;
\end{lstlisting}

\begin{figure}[h]
\centering
\begin{tikzpicture}[>=stealth,
	every loop/.style={out=105,in=75,looseness=0.15,->},
	level 1/.style={sibling distance=1.6cm},
	level 2/.style={sibling distance=1.2cm},
	every node/.style={draw,circle},
	edge from parent/.style={draw,<-}]
\node (a) {0}
	child {node[very thick] {1}
		child {node {2}}
		child {node {3}}
		child {node[very thick] {4}
			child {node {5}}
			child {node[very thick] (b) {6}}
		}
	};
\draw (b) edge[dashed, bend right=20,->] node[auto,swap,draw=none] {\footnotesize Find} (a);
\draw (a) edge[loop,out=105,in=75,looseness=4.2,->] (a);
\draw[->] (3,-2) -- ++(1,0);
\begin{scope}[xshift=7cm]
\node (c) {0}
	child {node[very thick] {1}
		child {node {2}}
		child {node {3}}
	}
	child {node[very thick] {4}
		child {node {5}}
	}
	child {node[very thick] {6}};
\draw (c) edge[loop,out=105,in=75,looseness=4.2,->] (c);
\end{scope}
\end{tikzpicture}
\caption{La compression de chemin en images.}
\end{figure}

\question
Écrire une fonction \texttt{initialise n} qui renvoie \texttt{uf} où \texttt{uf.pere} est le tableau initial où chaque élément est son propre père et \texttt{uf.rang}, le tableau initial où tous les rangs sont à 0.

\question
Écrire une fonction \texttt{trouver} telle que \texttt{trouver uf i} renvoie le représentant de la classe de \texttt{i} dans la structure \texttt{uf}, en effectuant la compression de chemin.

\question
Écrire une fonction \texttt{unir} telle que \texttt{unir uf i j} renvoie \texttt{false} si \texttt{i} et \texttt{j} sont déjà dans la même partition et réunit les partitions de \texttt{i} et de \texttt{j} puis renvoie \texttt{true} sinon. Distinguer les cas selon si le rang du représentant de \texttt{i} est plus petit, plus grand ou égal au rang du représentant de \texttt{j}, pour que votre fonction soit la plus efficace possible.

\question
Quelle est la complexité moyenne des fonctions \texttt{unir} et \texttt{trouver} ?

\question
Écrire une fonction qui prend en argument un graphe sous forme de liste d'arêtes et renvoie son nombre de composantes connexes. Allez, c'est bientôt Noël, on vous autorise à utiliser une référence pour le compteur.

\subsection{Algorithme de Kruskal}

On considère un graphe non orienté $G$ connexe et pondéré à poids positifs.

Un \emph{arbre couvrant} du graphe $G=(V, \rightarrow)$ est un arbre ${\cal A}=(V, \rightarrow')$ connexe tel que $\rightarrow'~\subset~\rightarrow$.
L'objectif de ce TP est aussi de trouver un arbre couvrant minimal (soit, de poids minimum) de $G$.\bigskip

L'algorithme de Kruskal construit un arbre couvrant minimal en procédant de la façon suivante :
\begin{itemize}
\item On trie les arêtes par poids croissant.
\item On considère chaque sommet comme un arbre à un élément.
\item Si une arête relie deux arbres distincts de la forêt, on ajoute cette arête à $\cal A$ et on fusionne les deux arbres, sinon on la jette.
\end{itemize}

\question
Montrer que l'algorithme de Kruskal termine.

%\question
%Écrire une fonction qui détermine le nombre de sommets $n$ d'un graphe connexe à partir de la liste de ses arêtes, de type \texttt{(int * int * int) list}.

\question
Implémenter l'algorithme de Kruskal à l'aide d'une structure union-find. Quelle est sa complexité ?

%\section{Tri topologique}
%
%% Jolie image + explication du tri topologique
%
%Le tri topologique d'un graphe est un classement de ses sommets de sorte que pour toute arête $uv$, $u$ soit avant $v$ dans le classement.
%
%\question
%Implémenter le tri topologique pour un graphe sous forme de listes d'adjacence.

\end{document}
