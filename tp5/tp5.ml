let initialise n =
	let pere = make_vect n 0 in
	for i = 0 to n - 1 do
		pere.(i) <- i;
	done;
	pere
;;

let rec trouver pere rang i =
	if pere.(i) = i then i
	else let _ = pere.(i) <- trouver pere rang pere.(i) in
		let _ = rang.(i) <- 0 in (* Ceci est parfaitement inutile *)
			pere.(i)
;;

let rec union pere rang i j =
	let k = trouver pere rang i and l = trouver pere rang j in
	if k != l then
		if rang.(k) < rang.(l) then
			pere.(k) <- l
		else if rang.(k) > rang.(l) then
			pere.(l) <- k
		else
			pere.(k) <- l;
			rang.(l) <- rang.(k) + 1
;;

if false then
	if true then print_string "Le chat est vivant."
else
	print_string "Le chat est mort."
;;

let graphe = [0,1,1;0,2,2;1,3,2;1,4,3;2,1,3];;

let kruskal graphe =
	let n = it_list (fun x (i,_,j) -> max x (max i j)) 0 graphe + 1 in
	let aretes = sort__sort (fun (_,a,_) (_,b,_) -> a < b) graphe in
	let rec aux pere rang arbre graphe = match graphe with
		| [] -> arbre
		| (i,p,j)::t -> if trouver pere rang i != trouver pere rang j then
			let _ = union pere rang i j in
			aux pere rang ((i,p,j)::arbre) t
		else
			aux pere rang arbre t
	in aux (initialise n) (make_vect n 0) [] aretes
;;

kruskal graphe;;




