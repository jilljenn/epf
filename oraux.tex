\documentclass[10pt,a5paper,landscape]{article}
\usepackage{oraux}
\usepackage{pgfpages}
\usepackage{stmaryrd}
\pgfpagesuselayout{2 on 1}[a4paper]

\def\A{\cal A}
\def\N{\mathbb{N}}

\def\title{Oral d'informatique fondamentale}

\begin{document}
\header

\section*{Recherche de motif dans un fichier compressé}

Soit $A = \{a, b, \ldots\}$ un alphabet fini. Un \emph{mot compressé} est une suite $W = w_1 \ldots w_n$ où chaque $w_i$ est soit une lettre $a_i$ de $A$, soit une paire d'entiers $(b_i, e_i)$ d'entiers satisfaisant une condition précisée plus bas.

Un mot compressé $W$ représente un mot $\widetilde{W} \in A^*$, son \emph{expansion}. On définit $\widetilde{w_1 \ldots w_n}$ comme la concaténation $u_1 \ldots u_n$ des $n$ mots donnés par
\[ u_i = \left\{\begin{array}{ll}
a_i & \textnormal{si } w_i = a_i \textnormal{ est une lettre},\\
(u_1 u_2 \ldots u_{i - 1})[b_i..e_i] & \textnormal{si } w_i \textnormal{ est une paire } (b_i, e_i).
\end{array}\right. \]

Ainsi $(b_i, e_i)$ représente la reprise d'un facteur (de longueur $e_i - b_i + 1$) du début $u_1 \ldots u_{i - 1}$ de l'expansion. On suppose que $W$ est \emph{bien formé}, c'est-à-dire que chaque paire de $W$ vérifie $1 \leqslant b_i \leqslant e_i \leqslant |u_1 \ldots u_{i - 1}|$.

\subsection*{Questions}

\begin{enumerate}
\item Calculez l'expansion de $W = ab(1, 2)(2, 3)c(1, 5)(4, 10)(1, 9)$.
\item Bornez la longueur de $\widetilde{W}$ en fonction de $n = |W|$.
\item Soit $m = m_1 \ldots m_r$ un mot de $r$ lettres, on voudrait déterminer si $m$ apparaît dans $\widetilde{W}$ sans devoir construire $\widetilde{W}$. On demande donc systématiquement des algorithmes polynomiaux en $n$ et $r$.\\
Donnez un algorithme disant si $m$ apparaît à la position $p$ dans $\widetilde{W}$, c'est-à-dire si $m = \widetilde{W}[p..p + r - 1]$.
\item Donnez un algorithme \texttt{zgrep}$(m, W)$ déterminant si $m$ apparaît quelque part dans $\widetilde{W}$.
\end{enumerate}

\header

\section*{Palindromes}

On considère un alphabet fini contenant au moins deux lettres $\Sigma = \{a, b, \ldots\}$. Un mot $w \in \Sigma^*$ est un \emph{palindrome} s'il coïncide avec son image miroir, comme par exemple les mots \og abba \fg, \og rotor \fg{} ou aussi le mot vide $\varepsilon$.

\subsection*{Questions}

\begin{enumerate}
\item Le langage des palindromes de $\Sigma^*$ est-il \emph{régulier} (on dit aussi \emph{rationnel}, ou \emph{reconnaissable}) ? Justifiez.
\item Pour un automate fini $\A$, on note $L(\A)$ le langage reconnu par $\A$. Proposez un algorithme qui, pour un automate (en général non déterministe) $\A$, dit si $L(\A)$ contient au moins un palindrome. Évaluez la complexité de votre algorithme en fonction de la taille de $\A$ (à définir).
\item Soit $\A$ un automate à $n$ états. Montrez que si $L(\A)$ contient un palindrome, alors il contient un palindrome de longueur inférieure à $2n^2$. Cette borne supérieure peut-elle être améliorée de façon significative ?
\end{enumerate}

\header

\section*{Langages clos par concaténation}

Un langage $L$ sur un alphabet $\Sigma$ est dit \emph{clos par concaténation} si $\forall u, v \in L, uv \in L$ et $\varepsilon \in L$. On dit alors que $x \in L$ est un \emph{générateur} si $x \neq \varepsilon$ et $(\exists u, v \in L, uv = x) \Rightarrow (u = \varepsilon \vee v = \varepsilon)$, et on note $G(L)$ l'ensemble des générateurs.

\subsection*{Questions}

{\small \begin{enumerate}
\item Soit $L$ un langage clos par concaténation. Montrer que $L = G(L)^*$.
\item Donner un exemple de langage $L$ rationnel et clos par concaténation tel que $G(L)$ soit infini.
\item Un langage $L$ clos par concaténation est dit libre s'il existe un alphabet $\Sigma'$ non nécessairement fini tel qu'il existe un isomorphisme pour la concaténation $\varphi : \Sigma'^* \rightarrow L$. Montrer que $L$ est libre si et seulement si $\forall u_1, \ldots, u_m, v_1, \ldots, v_n \in G(L), u_1 \ldots u_m = v_1 \ldots v_n \Rightarrow (m = n \wedge (\forall i \in \llbracket1, n\rrbracket, u_i = v_i)).$
\item Montrer que $L$ est libre si et seulement si $\forall w \in \Sigma^*, (\exists u, v \in L, uw, wv \in L \Rightarrow w \in L)$.
\end{enumerate}}

%\subsection*{Correction}
%
%{\footnotesize \begin{enumerate}
%\item $\supset$ par stabilité de la concaténation, $\subset$ par récurrence sur la longueur des mots.
%\item $L = (a^*b)^*$ est stable par concaténation. Montrons que les $a^n b$ sont générateurs pour $n \geqslant 0$. Si pour un tel $n$, $a^n b = uv$ avec $v \neq \varepsilon$, alors $u$ ne finit pas par $b$ et n'est donc pas dans $L$.
%\item $\Rightarrow$ : Supposons qu'il existe de tels $\Sigma'$ et $\varphi$. Soit $g \in G(L)$. $\varphi$ étant un morphisme surjectif, $\exists a_1 \ldots a_n \neq \varepsilon$ tel que $\varphi(a_1 \ldots a_n) = g$. Supposons par l'absurde $n \geqslant 2$. Alors $\varphi(a_1) \varphi(a_2 \ldots a_n) = g$, contradiction car $g$ est générateur.
%\item On utilise le résultat de la question précédente.\\
%$\Rightarrow$ : Soit $u, v, w \in L$ tels que $uw, wv \in L$. $L$ étant clos par concaténation, $(uw)v$ est dans $L$ donc admet une décomposition en générateurs $(g_1 \ldots g_i) g_{i + 1} \ldots g_m$. De même, $u(wv) \in L$ admet une décomposition $h_1 \ldots h_j (h_{j + 1} \ldots h_n)$. Par hypothèse, cette décomposition est unique donc $\exists k, l$ tels que $w = g_k \ldots g_l \in L$, ce qui conclut.\\
%$\Leftarrow$ : Supposons que par exemple $\exists i \in \llbracket1, n\rrbracket, \exists w, u_i w = v_i \in L$. Alors si on note $v$ tel que $wv = u_{i + 1} \ldots u_n \in L$, on a par hypothèse $w \in L$ qui se décompose en générateurs $w_1 \ldots w_t$. Donc il existe une décomposition en générateurs de $v_i = u_i w$ lui-même générateur, ce qui est une contradiction.
%\end{enumerate}}

\header

\section*{Représentations concises}

On s'intéresse à la compression de mots sur l'alphabet $\Sigma = \{0, 1\}$ grâce à des exposants. $\Sigma^*$ est l'ensemble des mots sur cet alphabet. Par exemple, pour le mot 00101010101111, on a les représentations $0^2(10)^41^4$, $0^1(01)^51^3$ et éventuellement d'autres.

On note $|x|$ la longueur d'un mot $x$, et on définit la taille d'une représentation $a_1^{\alpha_1} \ldots a_k^{\alpha_k}$ par $\sum_{i = 1}^k |a_i| + \lceil \log_2(\alpha_i) \rceil$. Enfin on note $\langle x \rangle$ la taille de la représentation de $x$ la plus concise, c'est-à-dire celle de taille minimale.

\subsection*{Questions}

\begin{enumerate}
\item Montrer qu'il existe une constante $c > 0$ telle que $\forall n \geqslant 0, \exists x \in \Sigma^*$ tel que $|x| = n$ et $\langle x \rangle \leqslant c \log_2(n)$.
\item Montrer qu'il n'existe pas de constante $d > 0$ telle que $\forall y \in \Sigma^*, \langle y \rangle \leqslant d \log_2(|y|)$.
\item Soit $L$ un langage rationnel, montrer qu'il existe des constantes $c, d > 0$ qui dépendent du langage $L$ telles que $\forall x \in L, \exists y \in L$ tel que $0 \leqslant |y| - |x| \leqslant d$ et $\langle y \rangle \leqslant c \log_2(|y|)$.
\item Soit $L$ un langage rationnel, montrer qu'il existe une constante $c > 0$ telle que $\forall x \in L$, il existe $y \in L$ tel que $|x| = |y|$ et $\langle y \rangle \leqslant c \log_2(|y|).$
\end{enumerate}

%\header
%
%\section*{Langages locaux}
%
%On dit qu'un langage $L$ est local s'il existe A, B et C tel que $L = X\Sigma^* \cap \Sigma^*B.$
%
%\subsection*{Questions}

\header

\section*{Fractions de langages}

Soit $L$ un langage. On note $\frac12 L = \{u | \exists v, uv \in L, |u| = |v|\}$.

\subsection*{Questions}

\begin{enumerate}
\item On considère $L = (a(aba + bab))^*$, déterminer $\frac12 L$.
\item Montrer que si $L$ est rationnel, $\frac12 L$ l'est aussi.
\item Soit $r \in \N^*$, on considère à présent $\frac1r L = \{u | \exists v, uv \in L, |u| = \frac1r |uv|\}$.\\
Montrer que si $L$ est rationnel, $\frac1r L$ l'est aussi.
\item Soit $p$ et $q$ deux entiers premiers entre eux. On note $\frac{p}q L = \{u | \exists v, uv \in L, |u| = \frac{p}q |uv|\}$.\\
Montrer que si $L$ est rationnel, $\frac{p}q L$ est rationnel.
\end{enumerate}

\end{document}  
