let rec fibo0 n = match n with
	| 0 -> 0
	| 1 -> 1
	| n -> fibo0 (n - 1) + fibo0 (n - 2)
;;

let fibogen f0 f1 op n = aux f0 f1 n where
	rec aux a b c = if c == 0 then a else aux b (op b a) (c - 1)
;;

let fibo = fibogen 0 1 prefix +;;
let fiboword = fibogen "b" "a" prefix ^;;
let fibolist0 = fibogen [1] [0] prefix @;;

fibo0 32;;
fibo 32;;
fiboword 5;;
fibolist0 5;;

let rec ajoute l1 l2 = match l1 with
	| [] -> l2
	| h::t -> ajoute t (h::l2)
;;

let remplace x lx l = aux [] l where
	rec aux r l = match l with
		| [] -> rev r
		| h::t -> if h == x then aux (ajoute lx r) t else aux (h::r) t
;;

remplace 2 [0] (remplace 0 [0;1] (remplace 1 [2] [0;1;0]));;

let rev1 l = aux l [] where
	rec aux l1 l2 = match l1 with
		| [] -> l2
		| h::t -> aux t (h::l2)
;;

rev1 [1;2;3];;

let rec exp0 a n =
	if n == 0 then 1
	else if n mod 2 == 0 then exp0 a (n / 2) * exp0 a (n / 2)
	else a * exp0 a (n / 2) * exp0 a (n / 2)
;;

let rec exp a n =
	if n == 0 then 1
	else if n mod 2 == 0 then exp (a * a) (n / 2)
	else a * exp (a * a) (n / 2)
;;

let exp2 a n = aux 1 a n where
	rec aux p a n = match n with
		| 0 -> p
		| n when n mod 2 == 0 -> aux p (a * a) (n / 2)
		| n -> aux (p * a) (a * a) (n / 2)
;;

exp0 2 1200;;
exp 1 1200;;
exp2 2 15;;

let nombre_occurrences x l = aux 0 l where
	rec aux nbOccurrences l = match l with
		| [] -> nbOccurrences
		| h::t -> aux (if h == x then nbOccurrences + 1 else nbOccurrences) t
;;

let test = [3; 4; 15; 9];;
let fibotest = map fibolist0 test;;
map (nombre_occurrences 0) fibotest;;
map fibo test;;

map (function x -> x * x) test;;

let map1 f l = aux [] l where
	rec aux r l = match l with
		| [] -> rev r
		| h::t -> aux (f h::r) t
;;

map1 (function x -> x * x) test;;

(*
nbocc
map
nbocc 0 fibolist fibo
rev
tronque
tronque 2 fibolist n
 *)
 
let tronque n l = aux n (rev l) where
	rec aux n l = match n with
		| 0 -> rev l
		| n -> aux (n - 1) (tl l)
;;

map1 (fun l -> l = rev l) (map1 (tronque 2) fibotest);;

let exp a n = aux 1 a n where
	rec aux p a n =
		if n == 0 then p
		else if n mod 2 == 0 then aux p (a * a) (n / 2)
		else aux (p * a) (a * a) (n / 2)
;;

(* Transforme un exposant en succession d'op�rations square et multiply *)
let n2sm n = aux [] n where
	rec aux l n =
		if n == 0 then it_list (fun x y -> x ^ y) "" l
		else if n mod 2 == 0 then aux ("S"::l) (n / 2)
		else aux ("S"::("M"::l)) (n / 2)
;;

n2sm 141;; (* SMSSSSMSMSSM *)
n2sm 280;; (* SMSSSSMSMSSS *)

(* Shamir chiffre x et -x, et �coute si les op�rations de square et multiply
font le m�me bruit (+) ou un bruit diff�rent (-) ; � partir de la s�quence
de + et -, retrouver les valeurs possibles de la cl� *)
let decode message =
	aux 0 (list_of_string [] (string_length message - 1)) where
		rec list_of_string l k = match k with
			| -1 -> l
			| _ -> list_of_string (message.[k]::l) (k - 1)
		and aux n l = match l with
			| [] -> n
			| _::(`-`::t) -> aux (n * 2 + 1) t
			| _::t -> aux (n * 2) t
;;

decode "---++--+---";; (* 75 *)
decode "---+---";; (* 11 *)
decode "---+--+";; (* 20 *)

(*
- + - + + - + -
 S M S S M S M
 ? ? ? = ? ? ?
 S M S S M S S
 ? ? ? = ? ? =
*)

let remplace_multi subst l = aux [] l where
	rec aux r l = match l with
		| [] -> rev r
		| h::t -> try let remp = assoc h subst in aux (ajoute remp r) t with Not_found -> aux (h::r) t
;;

remplace_multi [0,[0;1];1,[0]] [0;1;0];;
let test = [1;2;3;4;5];;
let fibotest = map fibolist0 test;;
tronque 1 (map (remplace_multi [0,[0;1];1,[0]]) fibotest) = tl fibotest;;

type op = S | M;;
let sm_of_exp n = aux [] n where
	rec aux l n = match n with
		| 0 -> l
		| n when n mod 2 == 0 -> aux (S::l) (n / 2)
		| n -> aux (S::(M::l)) (n / 2)
;;
sm_of_exp 27;;

let decode l = aux 0 l where
	rec aux n l = match l with
		| [] -> n
		| _::(false::t) -> aux (n * 2 + 1) t
		| _::t -> aux (n * 2) t
;;

decode [true;true;true;false;false;true;true;false;false;true;false];; (* 37 *)
decode [true;false;false;false;false;true;false;false;false];; (* 27 *)
decode [true;false;false;true;false;false;true];; (* 20 *)
decode [true;false;false;true;false;false;false];; (* 11 *)

let filter p l = aux [] l where
	rec aux r l = match l with
		| [] -> r
		| h::t -> if p h then aux (h::r) t else aux r t
;;

let nb_occ x0 l = list_length (filter (function x -> x == x0) l);;

nb_occ 3 [1;3;1;4];;



