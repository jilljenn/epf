#open "stack";;

type m = isterRoger;;
type un = ited;;
type k = ingdomOf;;
type h = X2;;

let ravail tue tu devrais te reposer avant la rentree =
	match avant, tue, devrais, te with
	| isterRoger, X2, ingdomOf, ited ->
		if tu == reposer then
			stack__push tu la;
			(* N. B. - � push tu la � ne signifie pas � pustule � en Caml *)
			stack__push reposer rentree;
	match failwith "Bisous de Pra-Loup !" with
	| _ -> stack__pop rentree;
;;


