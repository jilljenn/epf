type 'a arbre = Nil | Node of (char * 'a) * ('a arbre * 'a arbre);;

let phrase = "Le code de Huffman permet de compresser des donn�es � l'aide d'arbres binaires.";;

let init_list phrase =
	let n = string_length phrase in
	let rec ajoute l a = match l with
		| [] -> [a, 1]
		| h::t -> if fst h = a then (a, snd h + 1)::t else h::(ajoute t a)
	and compte l = function
		| i when i = n -> l
		| i -> compte (ajoute l phrase.[i]) (i + 1)
	in compte [] 0
;;

let l = map (fun x -> Node(x, (Nil, Nil))) (sort__sort (fun x y -> snd x < snd y) (init_list phrase));;

let rec huffman l =
	let rec insere l nx x = match l with
		| [] -> [x]
		| Node((_, n), (_, _))::_ when n >= nx -> x::l
		| h::t -> h::insere t nx x
	in match l with
		| [x] -> x
		| (Node ((_, n1), (_, _)) as h1)::(Node ((_, n2), (_, _)) as h2)::t -> huffman (insere t (n1 + n2) (Node((` `, n1 + n2), (h1, h2))))
		| _ -> Nil
;;

let arbre = huffman l;;

let table_huffman arbre =
	let rec parcours l etiquette arbre = match arbre with
		| Node((x, n), (Nil, Nil)) -> (x, etiquette)::l
		| Node((_, _), (t1, t2)) -> let l = parcours l (etiquette ^ "0") t1 in parcours l (etiquette ^ "1") t2
		| Nil -> []
	in parcours [] "" arbre
;;

let table = table_huffman arbre;;

let encode phrase table =
	let n = string_length phrase in
	let rec construit code = function
		| i when i = n -> code
		| i -> construit (code ^ (assoc phrase.[i] table)) (i + 1)
	in construit "" 0
;;

let message = encode phrase table;;

let decode message arbre =
	let n = string_length message in
	let rec parcours arbre0 reconstitution = function
		| i when i = n -> reconstitution
		| i -> match arbre0 with
			| Node((x, _), (Nil, Nil)) -> parcours arbre (reconstitution ^ (string_of_char x)) i
			| Node((_, _), (t1, t2)) -> parcours (if message.[i] = `0` then t1 else t2) reconstitution (i + 1)
			| Nil -> ""
	in parcours arbre "" 0
;;

decode message arbre;;

let h = hashtbl__new 50;;
hashtbl__add;;

