\documentclass[12pt]{article}
\usepackage{../tp}
\usepackage{multicol}
\usepackage{pgfplots}
\usepackage{adjustbox}

\def\title{Correction -- Statistical Data Processing}
\def\N{\mathcal{N}}
\def\E{\mathbb{E}}
\newcommand\statement[1]{\textcolor{gray}{#1}}
\newcommand\alert[1]{\textcolor{red}{#1}}

\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\begin{document}
\header

\section{Expectation}

\question \statement{Let $X$ be a discrete random variable that takes values 0, 1, 2 with probabilities 0.7, 0.2, 0.1 respectively. Let $Y = X^2 - 1$. Compute $\E(Y)$.}\bigskip

First of all, please note that: \alert{$\E(X^2) \neq {\E(X)}^2$}. Many people were mistaken because of this.

\[ \begin{aligned}
\E(Y) & = P(Y = 0^2 - 1)(0^2 - 1) + P(Y = 1^2 - 1)(1^2 - 1) + P(Y = 2^2 - 1)(2^2 - 1)\\
& = 0.7 \times -1 + 0.2 \times 0 + 0.1 \times 3\\
& = \alert{-0.4}.
\end{aligned} \]

\section{Washing Machine}

\statement{The amount of noise of a certain type of washing machine in a certain mode can be seen as a random variable of mean value 44 dB and standard deviation 5 dB.}

\question \statement{Using the Gaussian approximation, compute the probability of getting a mean value greater than 46 dB for a random sample of 10 machines.}

\adjustbox{valign=t}{\begin{minipage}{0.3\textwidth}
	\includegraphics{figures/q2}
\end{minipage}}
\adjustbox{valign=t}{\begin{minipage}{0.7\textwidth}
\begin{align*}
P(X > 46) & = P\left(Z > \frac{46 - 44}{5 / \sqrt{10}}\right)\\
& = P(Z > 0.126)\\
& = 0.5 - 0.39617\\
& = \alert{0.10383}.
\end{align*}
\end{minipage}}

\question \statement{Compute the probability of getting a mean value greater than 46 dB for a random sample of 25 machines.}

\adjustbox{valign=t}{\begin{minipage}{0.3\textwidth}
	\includegraphics{figures/q3}
\end{minipage}}
\adjustbox{valign=t}{\begin{minipage}{0.7\textwidth}
\begin{align*}
P(X > 46) & = P\left(Z > \frac{46 - 44}{5 / \sqrt{25}}\right)\\
& = P(Z > 2)\\
& = 0.5 - 0.47725\\
& = \alert{0.02275}.
\end{align*}
\end{minipage}}

\section{Insurance Company}

\statement{An insurance company must estimate, at the end of the year, the provision in the statement account regarding the current damages yet to be paid. To address this issue, they select at random 200 files and compute a mean value of 9,944 euros. The standard deviation is supposed to be 1,901 euros.}

\question \statement{Given that 11,210 files are being processed, compute a 95\% confidence interval over the total provision to incur.}

\[ IC_{95\%} = 11210 \times \left(9944 \pm 1.96 \cdot \frac{1901}{\sqrt{200}}\right) = \alert{[108518796, 114425684]}. \]

\question \statement{Compute a 99\% confidence interval.}\bigskip

You could have read as an indication in the cumulative from mean table that \alert{2.5758} corresponds to a probability of 0.49500 (so a 99\% confidence interval, by symmetry).

\[ IC_{99\%} = 11210 \times \left(9944 \pm 2.5758 \cdot \frac{1901}{\sqrt{200}}\right) = \alert{[107590872, 115353608]}. \]

\section{Factory}

\statement{A manufacturer must ship 100 items. Given that the production process generates a defect with probability 0.10, he wants to budget the number of items he should make in order to be almost sure to supply 100 good units. A simplistic argument leads him to state that 111 items are enough.}

\question \statement{What is the probability that 111 items are not enough to produce 100 good units?}\bigskip

\alert{Beware of rounding errors!} Some of you rounded 99.5/111 to 0.9 or 0.896, which led to completely different results. Anyway, I considered your answer correct if your reasoning was correct.\bigskip

Let $X_i$ be the gain of product $i$ (1 if good, 0 if defect). It is a Bernoulli variable of parameter $p = 0.9$, so it has mean value $\alert{\mu = 0.9}$ and standard deviation $\alert{\sigma = \sqrt{0.9(1 - 0.9)} = 0.3}$. The central limit theorem states that $\overline{X} = (X_1 + \ldots + X_{111})/111$ (which represents the rate of good products after 111 items have been made) behaves like a Gaussian variable of same parameters $\mu$ and $\sigma$.

\[ \begin{aligned}
P\left(\overline{X} \leqslant \frac{99.5}{111}\right) & = P\left(Z \leqslant \frac{99.5/111 - 0.9}{0.3 / \sqrt{111}}\right)\\
& = P(Z \leqslant -0.13)\\
& = 0.5 - 0.05172\\
& = \alert{0.44828}.
\end{aligned} \]

\question \statement{How many pieces should he make to insure that he gets 100 good units at the end?}\bigskip

You can \alert{never} be 100\% sure. For example, if you are really unlucky, all $n$ pieces can be defects (and this happens with probability $0.1^n$).

\question \statement{How many pieces should he make to get 100 good units with probability 99\%?}\bigskip

The probability that $n$ pieces won't be enough is: $P(Z \leqslant \frac{99.5/n - 0.9}{0.3/\sqrt{n}})$. So we want this probability to be below 0.01. But $\frac{99.5/n - 0.9}{0.3/\sqrt{n}}$ \alert{decreases} with $n$, so:

\[ \begin{aligned}
& P\left(Z \leqslant \frac{99.5/n - 0.9}{0.3/\sqrt{n}}\right) \leqslant 0.01\\
& \iff \frac{99.5/n - 0.9}{0.3/\sqrt{n}} \leqslant \alert{-2.5758}\\
& \iff 0.9n - 0.77274\sqrt{n} - 99.5 \geqslant 0.
\end{aligned} \]

This a quadratic equation $aX^2 + bX + c = 0$ and we know $aX^2 + bX + c$ has the same sign than $a$ (here $0.9 > 0$) except between its roots ($x_1$ and $x_2$). We keep here the positive solution $x = 10.9526$ which once squared gives $n = 119.95$. So we know $0.9n - 0.77274\sqrt{n} - 99.5 \geqslant 0$ when $n \geqslant 119.95$. Thus \alert{$n = 120$} pieces are enough to guarantee 100 good units with probability 99\%.

\section{Printing Jobs}

\statement{A service processing standard forms uses a network of ten computers and one printer. The mean waiting time for a form to be printed is 42.5 seconds (time between sending the printing job and getting the printed form) with a standard deviation of 8.2 seconds. Ten new computers and one printer are added to the network. Over thirty printing jobs in this new configuration, one has measured a mean time of 39.0 seconds.}

\question \statement{Describe a test of level 0.05 of the hypothesis that the mean printing time has not been affected by the network growth.}\bigskip

If the null hypothesis (``the mean printing time has not been affected'') is true, thus the computed value should be within $42.5 \pm 1.96 \cdot 8.2/\sqrt{30} = [39.57, 45.43]$ with confidence 95\%. Remember $n$ is the number of samples thus here the number of \alert{printing jobs}, not the number of computers or anything. (Many people made this mistake.)\bigskip

\adjustbox{valign=t}{\begin{minipage}{0.4\textwidth}
	\includegraphics{figures/q9}
\end{minipage}}
\adjustbox{valign=t}{\begin{minipage}{0.6\textwidth}\parindent11.10839pt
\noindent Thus, the test is the following:\bigskip

\noindent \alert{If $\overline{X} \in [39.57, 45.43]$\\
\indent\indent\textsf{ACCEPT} the null hypothesis\\
Else\\
\indent\indent\textsf{REJECT}.}
\end{minipage}}

\question \statement{What is the $p$-value associated to those results?}\bigskip

The probability to observe a value at least as extreme as 39.0 is:

\adjustbox{valign=t}{\begin{minipage}{0.3\textwidth}
	\includegraphics{figures/q10}
\end{minipage}}
\adjustbox{valign=t}{\begin{minipage}{0.7\textwidth}
\begin{align*}
P(\overline{X} \leqslant 39.0) & = P\left(Z \leqslant \frac{39.0 - 42.5}{8.2 / \sqrt{30}}\right)\\
& = P(Z \leqslant -2.34)\\
& = 0.5 - 0.49036\\
& = \alert{0.00964}.
\end{align*}
\end{minipage}}

It is below 0.05, so the null hypothesis is \alert{rejected} (anyway, it wasn't in the confidence interval).\bigskip

… \textbf{By the way,} the distribution of the notes of the exam looks like a Gaussian distribution:
\begin{center}
\includegraphics[width=3cm]{figures/notes}
\end{center}

\end{document}
