let rec ins�re compare l e = match l with
	| [] -> [e]
	| h::t when compare e h -> e::h::t
	| h::t -> h::ins�re compare t e
;;

let extrait_min = function
	| [] -> invalid_arg "extract_min"
	| h::t -> h, t
;;

let graphe = [|[1,1;2,2];[2,3;3,4];[3,1];[]|];;

let prim graphe =
	let n = vect_length graphe in
	let visit� = make_vect n false in
	visit�.(0) <- true;
	let rec ajouter tas voisins j = match voisins with (* ajoute au tas les ar�tes partant de j *)
		| [] -> tas
		| (k,p)::t -> ajouter (ins�re (fun (a,_,_) (b,_,_) -> a < b) tas (p,j,k)) t j (* sous la forme (poids, d�part (j), arriv�e) *)
	in let rec aux arbre tas =
		if tas = [] then arbre else (* plus de tas, on a fini *)
		let (p,i,j), tas = extract_min tas in
		if not visit�.(j) then ( (* si cette ar�te ne joint pas deux sommets de l'arbre *)
			visit�.(j) <- true;
			aux ((i,j)::arbre) (ajouter tas graphe.(j) j) (* on ajoute l'ar�te � l'arbre et on continue avec le tas augment� des voisins de j *)
		) else (* sinon on jette l'ar�te *)
			aux arbre tas
	in aux [] (ajouter [] graphe.(0) 0) (* le tas est initialis� aux voisins de 0, mais �a marche aussi avec 1 et peut-�tre m�me avec 2 *)
;;

prim graphe;;

