let q2 () =
  tri_selection [] = []
  && tri_selection [1;2;3;42;5;6;7] = [1;2;3;5;6;7;42]
  && tri_selection [1;3;3;7] = [1;3;3;7]
  && tri_selection [9;8;7;6;5;4;3;2;2;1;0] = [0;1;2;2;3;4;5;6;7;8;9]
;;

let q3 () =
  rendu_glouton [3;2;1] 12345678910 = [4115226303;0;1]
  && rendu_glouton [200; 100; 50; 20; 10; 5; 2; 1] 1337 = [6; 1; 0; 1; 1; 1; 1; 0]
  && rendu_glouton [6;4;1] 8 = [1;0;2]
;;

let q5 () =
  not (est_representable [] 42)
  && est_representable [] 0
  && est_representable [342; 25] 391481
  && not (est_representable [2; 4; 6; 8] 123457)
;;
