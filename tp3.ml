let flatten = it_list (fun x y -> x @ y) [];; (* Beurk *)

let rec flatten l = match l with
	| [] -> []
	| h::t -> it_list (fun x y -> y::x) (flatten t) (rev h)
;;

flatten [[1;2];[3];[4;5]];;

let rec est_sous_liste u v = match u, v with
	| [], _ -> true
	| _, [] -> false
	| h::t, h2::t2 -> (h == h2 && est_sous_liste t t2) || est_sous_liste (h::t) t2
;;

est_sous_liste [2;4] [2;3];;
est_sous_liste [1;2;4] [1;2;3;4];;

let premiere_occurrence x l = aux 0 l where
	rec aux index l = match l with
		| [] -> failwith "Hey, WTF"
		| h::t -> if h == x then index else aux (index + 1) t
;;

premiere_occurrence 4 [3; 1; 4];;

let nombre_occurrences x l = aux 0 l where
	rec aux nbOccurrences l = match l with
		| [] -> nbOccurrences
		| h::t -> aux (if h == x then nbOccurrences + 1 else nbOccurrences) t
;;

let list_of_string s = rev it_list (fun x y -> y::x) [];;

nombre_occurrences `r` [3; 1; 4];;

let range n = fold_left (fun x y -> (hd x + 1)::x) [0] rev (list_of_vect (make_vect n 0));;

(*
fold
map

rev

premi�re occurrence
nombre occurrences
*)



